package statements

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenExistSQL(t *testing.T) {
	statement, err := createTestStatement()
	assert.NoError(t, err)

	statement.RefTable = nil
	statement.SetTable("testDB")
	statement.Alias("tdb")
	statement.Where(`tdb.id=1`)
	sql, _, err := statement.GenExistSQL()
	assert.NoError(t, err)
	assert.Equal(t, "SELECT 1 FROM `testDB` AS `tdb` WHERE (tdb.id=1) LIMIT 1", sql)
}
